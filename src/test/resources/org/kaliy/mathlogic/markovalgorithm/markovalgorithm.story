Scenario: Simple string replacement
Given substitutions are:
a->1,
b->2,
c->3
And alphabet is: a,b,c
And input string is: aabbcc
When Markov algorithm interpreter runs in verbose mode
Then steps are:
1abbcc,
11bbcc,
112bcc,
1122cc,
11223c,
112233,

Scenario: String replacement with stop symbol
Given substitutions are:
a->1,
b->2~,
c->3
And alphabet is: a,b,c
And input string is: aabbcc
When Markov algorithm interpreter runs in verbose mode
Then steps are:
1abbcc,
11bbcc,
112bcc

Scenario: Adding 11 to string of 1
Given substitutions are:
1->111~
And alphabet is: 1
And input string is: 111
When Markov algorithm interpreter runs in verbose mode
Then steps are:
11111

Scenario: converting binary to unary numeral system
!--https://ru.wikipedia.org/wiki/%D0%9D%D0%BE%D1%80%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC#.D0.9F.D1.80.D0.B8.D0.BC.D0.B5.D1.80_1
Given substitutions are:
1->0|,
|0->0||,
0->
And alphabet is: 0,1,|
And input string is: 101
When Markov algorithm interpreter runs in verbose mode
Then steps are:
0|01,
0|00|,
00||0|,
00|0|||,
000|||||,
00|||||,
0|||||,
|||||

Scenario: Sorting string that cosists of a,b,c
!--https://ru.wikipedia.org/wiki/%D0%9D%D0%BE%D1%80%D0%BC%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC#.D0.9F.D1.80.D0.B8.D0.BC.D0.B5.D1.80_1
Given substitutions are:
ba->ab,
ca->ac,
cb->bc
And alphabet is: a,b,c
And input string is: cbabca
When Markov algorithm interpreter runs in verbose mode
Then steps are:
cabbca,
acbbca,
acbbac,
acbabc,
acabbc,
aacbbc,
aabcbc,
aabbcc
