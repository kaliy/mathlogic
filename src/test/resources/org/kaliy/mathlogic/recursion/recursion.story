Scenario: testing recursion calculations
Given x and y are integers
Then calculations for them will be:
| x | y | sum | subtraction | multiplication | power |
| 11 | 2 | 13 | 9 | 22 | 121 |
| -11 | 2 | -9 | -13 | -22 | 121 |
| 11 | 0 | 11 | 11 | 0 | 1 |
| 0 | 11 | 11 | -11 | 0 | 0 |