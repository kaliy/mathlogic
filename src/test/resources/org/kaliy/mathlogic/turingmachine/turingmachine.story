Scenario: simple incrementer
Given blank symbol is: b
And initial state is: q0
And initial tape is: 111
And terminal states are: qf
And transitions are:
q01->q01R,
q0b->qf1
When Turing Machine interpreter runs in verbose mode
Then steps are:
| Machine state | Current transition |
| [ [1] 1 1 ] | (q0,1)=>(q0,1)/RIGHT |
| [ [1] 1 1 ] | (q0,1)=>(q0,1)/RIGHT |
| [ 1 [1] 1 ] | (q0,1)=>(q0,1)/RIGHT |
| [ 1 1 [1] b ] | (q0,b)=>(qf,1)/NEUTRAL |
| [ 1 1 [1] 1 ] | (qf,1)=>null/NEUTRAL |

Scenario: Add 1 to binary digit
Given blank symbol is: b
And initial state is: q1
And initial tape is: 10111
And terminal states are: qf
And transitions are:
q11->q11R,
q10->q10R,
q1b->q2bL,
q21->q20L,
q2b->qf1,
q20->q31L,
q30->q30L,
q31->q31L,
q3b->qfbR
When Turing Machine interpreter runs in verbose mode
Then steps are:
| Machine state | Current transition |
| [ [1] 0 1 1 1 ] | (q1,1)=>(q1,1)/RIGHT |
| [ [1] 0 1 1 1 ] | (q1,0)=>(q1,0)/RIGHT |
| [ 1 [0] 1 1 1 ] | (q1,1)=>(q1,1)/RIGHT |
| [ 1 0 [1] 1 1 ] | (q1,1)=>(q1,1)/RIGHT |
| [ 1 0 1 [1] 1 ] | (q1,1)=>(q1,1)/RIGHT |
| [ 1 0 1 1 [1] b ] | (q1,b)=>(q2,b)/LEFT |
| [ 1 0 1 [1] 1 b ] | (q2,1)=>(q2,0)/LEFT |
| [ 1 0 [1] 1 0 b ] | (q2,1)=>(q2,0)/LEFT |
| [ 1 [0] 1 0 0 b ] | (q2,1)=>(q2,0)/LEFT |
| [ [1] 0 0 0 0 b ] | (q2,0)=>(q3,1)/LEFT |
| [ [1] 1 0 0 0 b ] | (q3,1)=>(q3,1)/LEFT |
| [ [b] 1 1 0 0 0 b ] | (q3,b)=>(qf,b)/RIGHT |
| [ [b] 1 1 0 0 0 b ] | (qf,1)=>null/NEUTRAL |