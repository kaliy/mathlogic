package org.kaliy.mathlogic.recursion;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.kaliy.mathlogic.markovalgorithm.MarkovAlgorithmInterpreter;
import org.kaliy.mathlogic.markovalgorithm.MarkovAlgorithmSubstitution;
import org.kaliy.mathlogic.markovalgorithm.MarkovAlgorithmSubstitutionFactory;
import org.kaliy.mathlogic.turingmachine.MachineResult;

import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static org.assertj.core.api.Assertions.assertThat;

public class RecursionSteps {

    private int x,y;

    @Given("x and y are integers")
    public void xAndYAreIntegers() {
    }

    @Then("calculations for them will be: $calculations")
    public void checkCanculations(ExamplesTable calculations) {
        for (int i = 0; i < calculations.getRowCount(); i++) {
            Map<String,String> row = calculations.getRow(i);
            int x = parseInt(row.get("x"));
            int y = parseInt(row.get("y"));
            assertThat(Recursion.add(x, y)).isEqualTo(parseInt(row.get("sum")));
            assertThat(Recursion.subtraction(x, y)).isEqualTo(parseInt(row.get("subtraction")));
            assertThat(Recursion.multiply(x, y)).isEqualTo(parseInt(row.get("multiplication")));
            assertThat(Recursion.power(x, y)).isEqualTo(parseInt(row.get("power")));
        }
    }


}
