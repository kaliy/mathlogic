package org.kaliy.mathlogic.recursion;

import org.kaliy.mathlogic.StoriesBase;
import org.kaliy.mathlogic.markovalgorithm.MarkovAlgorithmSteps;

public class RecursionStories extends StoriesBase {
    protected Object getStepsObject() {
        return new RecursionSteps();
    }

    protected String getStoriesPath() {
        return "org/kaliy/mathlogic/recursion/recursion.story";
    }

}
