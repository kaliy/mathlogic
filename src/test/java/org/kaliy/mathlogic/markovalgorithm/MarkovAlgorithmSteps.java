package org.kaliy.mathlogic.markovalgorithm;

import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MarkovAlgorithmSteps {

    MarkovAlgorithmInterpreter interpreter;
    private boolean verbose = false;

    @BeforeScenario
    public void setUp() {
        interpreter = new MarkovAlgorithmInterpreter();
    }

    @Given("substitutions are: $substitutions")
    public void addSubstitutions(List<String> substitutions) {
        for (String substitution: substitutions) {
            MarkovAlgorithmSubstitution mas = MarkovAlgorithmSubstitutionFactory.fromString(substitution);
            interpreter.addSubstitution(mas);
        }
    }

    @Given("alphabet is: $alphabet")
    public void addAlphabet(List<String> alphabet) {
        for (String character: alphabet) {
            System.out.println("LAL " + character);
            interpreter.addToAphabet(character);
        }
    }

    @Given("input string is: $inputString")
    public void setInputString(String inputString) {
        interpreter.setInputString(inputString);
    }

    @When("Markov algorithm interpreter runs in verbose mode")
    public void turnOnVerboseMode() {
        verbose = true;
    }

    @Then("steps are: $steps")
    public void checkSteps(List<String> steps) {
        for (String step: steps) {
            assertThat(interpreter.nextStep()).isEqualTo(step);
        }
        assertThat(interpreter.nextStep()).isNull();
    }


}
