package org.kaliy.mathlogic.markovalgorithm;

import org.kaliy.mathlogic.StoriesBase;

public class MarkovStories extends StoriesBase {
    protected Object getStepsObject() {
        return new MarkovAlgorithmSteps();
    }

    protected String getStoriesPath() {
        return "org/kaliy/mathlogic/markovalgorithm/markovalgorithm.story";
    }

}
