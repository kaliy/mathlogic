package org.kaliy.mathlogic.turingmachine;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class TuringMachineSteps {

    TuringMachine machine;
    private boolean verbose = false;

    @BeforeScenario
    public void setUp() {
        machine = new TuringMachine();
    }

    @Given("blank symbol is: $blankSymbol")
    public void addBlankSymbol(String blankSymbol) {
        machine.setBlankSymbol(blankSymbol);
    }

    @Given("transitions are: $transitions")
    public void addTransitions(List<String> transitions) {
        Set<Transition> t = new HashSet<Transition>(transitions.size());
        for (String transition: transitions) {
            t.add(Transition.valueOf(transition));
        }
        machine.setTransitions(t);
    }

    @Given("initial state is: $initialState")
    public void setInitialState(String initialState) {
        machine.setInitialState(initialState);
    }

    @Given("initial tape is: $initialState")
    public void initializeMachine(String initialTape) {
        machine.initializeTape(initialTape);
    }


    @Given("terminal states are: $terminalStates")
    public void setTerminalStates(List<String> terminalStates) {
        Set<String> states = new HashSet<String>();
        states.addAll(terminalStates);
        machine.setTerminalStates(states);
    }


    @When("Turing Machine interpreter runs in verbose mode")
    public void turnOnVerboseMode() {
        verbose = true;
    }

    @Then("steps are: $steps")
    public void checkSteps(ExamplesTable steps) {
        List<MachineResult> results = machine.runTM();
        for (int i = 0; i < steps.getRowCount(); i++) {
            MachineResult result = results.get(i);
            Map<String,String> row = steps.getRow(i);
            assertThat(result.getMachineStateAsString()).isEqualTo(row.get("Machine state"));
            assertThat(result.getCurrentTransition().toString()).isEqualTo(row.get("Current transition"));
        }
    }


}
