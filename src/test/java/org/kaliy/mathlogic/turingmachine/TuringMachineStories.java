package org.kaliy.mathlogic.turingmachine;

import org.kaliy.mathlogic.StoriesBase;
import org.kaliy.mathlogic.markovalgorithm.MarkovAlgorithmSteps;

public class TuringMachineStories extends StoriesBase {
    protected Object getStepsObject() {
        return new TuringMachineSteps();
    }

    protected String getStoriesPath() {
        return "org/kaliy/mathlogic/turingmachine/turingmachine.story";
    }

}
