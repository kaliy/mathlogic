package org.kaliy.mathlogic.turingmachine;

import java.util.*;
import static org.kaliy.mathlogic.turingmachine.Direction.LEFT;
import static org.kaliy.mathlogic.turingmachine.Direction.RIGHT;
import static org.kaliy.mathlogic.turingmachine.Direction.NEUTRAL;

public class TuringMachine {
    private List<String> tape;
    private String blankSymbol;
    private ListIterator<String> head;
    private Map<TapeState, Transition> transitions = new HashMap<TapeState, Transition>();
    private Set<String> terminalStates;
    private String initialState;

    public TuringMachine() {
    }

    public void initializeTape(String input) {
        tape = new LinkedList<String>();
        for (int i = 0; i < input.length(); i++) {
            tape.add(input.charAt(i) + "");
        }
    }


    public TuringMachine nextStep() {
        return null;
    }

    public List<MachineResult> runTM() {
        List<MachineResult> result = new ArrayList<MachineResult>();
        if (tape.size() == 0) {
            tape.add(blankSymbol);
        }

        head = tape.listIterator();
        head.next();
        head.previous();

        TapeState tsp = new TapeState(initialState, tape.get(0));

        while (transitions.containsKey(tsp)) {
            result.add(new MachineResult(this.toString(), transitions.get(tsp)));
            Transition trans = transitions.get(tsp);
            head.set(trans.getTo().getTapeSymbol());
            tsp.setState(trans.getTo().getState());
            if (trans.getDirection() == Direction.LEFT) {
                if (!head.hasPrevious()) {
                    head.add(blankSymbol);
                }
                tsp.setTapeSymbol(head.previous());
            } else if (trans.getDirection() == Direction.RIGHT) {
                head.next();
                if (!head.hasNext()) {
                    head.add(blankSymbol);
                    head.previous();
                }
                tsp.setTapeSymbol(head.next());
                head.previous();
            } else {
                tsp.setTapeSymbol(trans.getTo().getTapeSymbol());
            }
        }

        result.add(new MachineResult(this.toString(), new Transition(tsp, null, NEUTRAL)));
        return result;
    }

    @Override
    public String toString() {
        try {
            int headPos = head.previousIndex() > -1 ? head.previousIndex() : 0;
            StringBuilder sb = new StringBuilder();
            sb.append("[ ");

            for (int i = 0; i < headPos; i++) {
                sb.append(tape.get(i)).append(" ");
            }

            sb.append("[").append(tape.get(headPos)).append("] ");

            for (int i = headPos + 1; i < tape.size(); i++) {
                sb.append(tape.get(i)).append(" ");
            }
            sb.append("]");
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void main(String[] args) {
        String init = "q0";
        String blank = "b";

        Set<String> term = new HashSet<String>();
        term.add("qf");

        Set<Transition> trans = new HashSet<Transition>();

        trans.add(Transition.valueOf("q01->q01R"));
        trans.add(Transition.valueOf("q0b->qf1"));

        TuringMachine machine = new TuringMachine();
        machine.setBlankSymbol(blank);
        machine.setTransitions(trans);
        machine.setInitialState(init);
        machine.setTerminalStates(term);
        machine.initializeTape("111");
        System.out.println("Output (si): ");
        printMachineResults(machine.runTM());

        init = "a";

        term.clear();
        term.add("halt");

        blank = "0";

        trans.clear();

        trans.add(new Transition(new TapeState("a", "0"), new TapeState("b", "1"), RIGHT));
        trans.add(new Transition(new TapeState("a", "1"), new TapeState("c", "1"), LEFT));
        trans.add(new Transition(new TapeState("b", "0"), new TapeState("a", "1"), LEFT));
        trans.add(new Transition(new TapeState("b", "1"), new TapeState("b", "1"), RIGHT));
        trans.add(new Transition(new TapeState("c", "0"), new TapeState("b", "1"), LEFT));
        trans.add(new Transition(new TapeState("c", "1"), new TapeState("halt", "1"), NEUTRAL));

        machine = new TuringMachine();
        machine.setBlankSymbol(blank);
        machine.setTransitions(trans);
        machine.setInitialState(init);
        machine.setTerminalStates(term);
        machine.initializeTape("");
        System.out.println("Output (bb): ");
        printMachineResults(machine.runTM());

        init = "s0";
        blank = "*";

        term = new HashSet<String>();
        term.add("see");

        trans = new HashSet<Transition>();

        trans.add(new Transition(new TapeState("s0", "a"), new TapeState("s0", "a"), RIGHT));
        trans.add(new Transition(new TapeState("s0", "b"), new TapeState("s1", "B"), RIGHT));
        trans.add(new Transition(new TapeState("s0", "*"), new TapeState("se", "*"), LEFT));
        trans.add(new Transition(new TapeState("s1", "a"), new TapeState("s1", "a"), RIGHT));
        trans.add(new Transition(new TapeState("s1", "b"), new TapeState("s1", "b"), RIGHT));
        trans.add(new Transition(new TapeState("s1", "*"), new TapeState("s2", "*"), LEFT));
        trans.add(new Transition(new TapeState("s2", "a"), new TapeState("s3", "b"), LEFT));
        trans.add(new Transition(new TapeState("s2", "b"), new TapeState("s2", "b"), LEFT));
        trans.add(new Transition(new TapeState("s2", "B"), new TapeState("se", "b"), LEFT));
        trans.add(new Transition(new TapeState("s3", "a"), new TapeState("s3", "a"), LEFT));
        trans.add(new Transition(new TapeState("s3", "b"), new TapeState("s3", "b"), LEFT));
        trans.add(new Transition(new TapeState("s3", "B"), new TapeState("s0", "a"), RIGHT));
        trans.add(new Transition(new TapeState("se", "a"), new TapeState("se", "a"), LEFT));
        trans.add(new Transition(new TapeState("se", "*"), new TapeState("see", "*"), RIGHT));

        machine = new TuringMachine();
        machine.setBlankSymbol(blank);
        machine.setTransitions(trans);
        machine.setInitialState(init);
        machine.setTerminalStates(term);

        machine.initializeTape("babbababaa");
        System.out.println("Output (sort): ");
        printMachineResults(machine.runTM());

        machine = new TuringMachine();
        machine.setBlankSymbol("b");
        term = new HashSet<String>();
        term.add("qf");
        machine.setTerminalStates(term);
        machine.setInitialState("q1");
        trans = new HashSet<Transition>();
        trans.add(Transition.valueOf("q11->q11R"));
        trans.add(Transition.valueOf("q10->q10R"));
        trans.add(Transition.valueOf("q1b->q2bL"));
        trans.add(Transition.valueOf("q21->q20L"));
        trans.add(Transition.valueOf("q2b->qf1"));
        trans.add(Transition.valueOf("q20->q31L"));
        trans.add(Transition.valueOf("q30->q30L"));
        trans.add(Transition.valueOf("q31->q31L"));
        trans.add(Transition.valueOf("q3b->qfbR"));
        machine.setTransitions(trans);
        machine.initializeTape("10111");

        System.out.println("Output (sort): ");

        printMachineResults(machine.runTM());


    }

    private static void printMachineResults(List<MachineResult> results) {
        for (MachineResult result: results) {
            System.out.println("| " + result.getMachineStateAsString() + " | " + result.getCurrentTransition() + " |");
        }
    }

    public void setBlankSymbol(String blankSymbol) {
        this.blankSymbol = blankSymbol;
    }

    public void setTransitions(Set<Transition> transitions) {
        for (Transition t : transitions) {
            this.transitions.put(t.getFrom(), t);
        }
    }

    public void setTerminalStates(Set<String> terminalStates) {
        this.terminalStates = terminalStates;
    }

    public void setInitialState(String initialState) {
        this.initialState = initialState;
    }
}
