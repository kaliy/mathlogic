package org.kaliy.mathlogic.turingmachine;

public class TapeState {
    private String state;
    private String tapeSymbol;

    public TapeState(String state, String tapeSymbol) {
        this.setState(state);
        this.setTapeSymbol(tapeSymbol);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((getState() == null) ? 0 : getState().hashCode());
        result = prime
                * result
                + ((getTapeSymbol() == null) ? 0 : getTapeSymbol()
                .hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TapeState other = (TapeState) obj;
        if (getState() == null) {
            if (other.getState() != null)
                return false;
        } else if (!getState().equals(other.getState()))
            return false;
        if (getTapeSymbol() == null) {
            if (other.getTapeSymbol() != null)
                return false;
        } else if (!getTapeSymbol().equals(other.getTapeSymbol()))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "(" + getState() + "," + getTapeSymbol() + ")";
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTapeSymbol() {
        return tapeSymbol;
    }

    public void setTapeSymbol(String tapeSymbol) {
        this.tapeSymbol = tapeSymbol;
    }
}
