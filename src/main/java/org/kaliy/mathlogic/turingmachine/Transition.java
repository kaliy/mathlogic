package org.kaliy.mathlogic.turingmachine;

public class Transition {
    private TapeState from;
    private TapeState to;
    private Direction direction;

    public Transition(TapeState from, TapeState to, Direction direction) {
        this.setFrom(from);
        this.setTo(to);
        this.setDirection(direction);
    }

    @Override
    public String toString() {
        return getFrom() + "=>" + getTo() + "/" + getDirection();
    }

    public TapeState getFrom() {
        return from;
    }

    public void setFrom(TapeState from) {
        this.from = from;
    }

    public TapeState getTo() {
        return to;
    }

    public void setTo(TapeState to) {
        this.to = to;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public static Transition valueOf(String string) {
        String[] s = string.split("->");
        String tapeSymbolFrom = s[0].substring(s[0].length() - 1);
        String stateFrom = s[0].substring(0, s[0].length() - 1);
        Direction direction = Direction.fromLetterCode(s[1].substring(s[1].length() - 1));
        String tapeSymbolTo,stateTo;
        if (direction == Direction.LEFT || direction == Direction.RIGHT) {
            stateTo = s[1].substring(0, s[1].length() - 2);
            tapeSymbolTo = s[1].substring(s[1].length() - 2, s[1].length() - 1);
        } else {
            stateTo = s[1].substring(0, s[1].length() - 1);
            tapeSymbolTo = s[1].substring(s[1].length() - 1);
        }
        return new Transition(
                new TapeState(stateFrom, tapeSymbolFrom),
                new TapeState(stateTo, tapeSymbolTo),
                direction
        );
    }
}
