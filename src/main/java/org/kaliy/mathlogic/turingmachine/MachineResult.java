package org.kaliy.mathlogic.turingmachine;

public class MachineResult {

    private String machineStateAsString;
    private Transition currentTransition;

    public MachineResult(String machineStateAsString, Transition currentTransition) {
        this.machineStateAsString = machineStateAsString;
        this.currentTransition = currentTransition;
    }

    public String getMachineStateAsString() {
        return machineStateAsString;
    }

    public Transition getCurrentTransition() {
        return currentTransition;
    }

    @Override
    public String toString() {
        return "[" + machineStateAsString + "; " + currentTransition + ']';
    }
}
