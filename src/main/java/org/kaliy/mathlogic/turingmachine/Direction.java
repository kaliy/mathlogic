package org.kaliy.mathlogic.turingmachine;

public enum Direction {
    LEFT(-1), RIGHT(+1), NEUTRAL(0);
    private int code;
    private Direction(int code) {
        this.code = code;
    }
    public static Direction valueOf(int code) {
        for(Direction direction : values()) {
            if (direction.code == code) {
                return direction;
            }
        }
        return null;
    }
    public static Direction fromLetterCode(String code) {
        if (code.equalsIgnoreCase("r")) {
            return RIGHT;
        } else if (code.equalsIgnoreCase("l")) {
            return LEFT;
        }
        return NEUTRAL;
    }
}
