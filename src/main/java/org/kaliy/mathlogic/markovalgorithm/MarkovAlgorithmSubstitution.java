package org.kaliy.mathlogic.markovalgorithm;

public class MarkovAlgorithmSubstitution {
    private String from;
    private String to;
    private boolean finalSubstitution = false;

    public MarkovAlgorithmSubstitution(String from, String to) {
        this(from, to, false);
    }

    public MarkovAlgorithmSubstitution(String from, String to, boolean finalSubstitution) {
        this.from = from;
        this.to = to;
        this.finalSubstitution = finalSubstitution;
    }

    public String toString() {
        return from + "->" + to + (finalSubstitution ? "~" : "");
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public boolean isFinalSubstitution() {
        return finalSubstitution;
    }
}
