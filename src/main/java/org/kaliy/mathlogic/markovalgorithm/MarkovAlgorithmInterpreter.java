package org.kaliy.mathlogic.markovalgorithm;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MarkovAlgorithmInterpreter {

    private List<MarkovAlgorithmSubstitution> substitutions = new ArrayList<MarkovAlgorithmSubstitution>();
    private Set<String> alphabet = new HashSet<String>();
    private String inputString;
    private boolean finished = false;

    public String nextStep() {
        if (finished) {
            return null;
        }
        for (MarkovAlgorithmSubstitution substitution: substitutions) {
            if (inputString.contains(substitution.getFrom())) {
                inputString = substitute(substitution);
                if (substitution.isFinalSubstitution()) {
                    finished = true;
                }
                return inputString;
            }
        }
        return null;
    }

    private String substitute(MarkovAlgorithmSubstitution substitution) {
        String replaced = StringUtils.replaceOnce(inputString, substitution.getFrom(), substitution.getTo());
        if (substitution.isFinalSubstitution()) {
            finished = true;
        }
        return replaced;
    }

    public void addToAphabet(String string) {
        alphabet.add(string);
    }

    public void addSubstitution(MarkovAlgorithmSubstitution substitution) {
        substitutions.add(substitution);
    }

    public void setInputString(String inputString) {
        for (char character: inputString.toCharArray()) {
            if (!alphabet.contains(Character.toString(character))) {
                throw new IllegalArgumentException("Illegal character");
            }
        }
        this.inputString = inputString;
    }

}
