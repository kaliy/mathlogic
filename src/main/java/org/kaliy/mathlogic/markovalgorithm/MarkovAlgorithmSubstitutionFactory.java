package org.kaliy.mathlogic.markovalgorithm;

public class MarkovAlgorithmSubstitutionFactory {
    public static MarkovAlgorithmSubstitution fromString(String substitution) {
        String[] ruleString = substitution.split("->");
        String from = ruleString[0];
        String to;
        if (substitution.trim().endsWith("->")) {
            to = "";
        } else {
            to = ruleString[1];
        }
        boolean end = false;
        if (ruleString.length > 1 && ruleString[1].endsWith("~")) {
            end = true;
            to = ruleString[1].substring(0, ruleString[1].length() - 1);
        }
        return new MarkovAlgorithmSubstitution(from, to, end);
    }
}
