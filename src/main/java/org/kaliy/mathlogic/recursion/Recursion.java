package org.kaliy.mathlogic.recursion;

public class Recursion {

    public static void main(String[] args) {
        int x = 11;
        int y = 2;
        System.out.println("Given x= " + x);
        System.out.println("And y= " + y);
        System.out.println("x+y: " + add(x, y));
        System.out.println("x*y: " + multiply(x, y));
        System.out.println("x^y: " + power(x, y));
        System.out.println("x-1: " + subtraction(x, 1));
    }

    public static int multiply(int x, int y) {
        if (x == 0 || y == 0) {
            return 0;
        }
        else if( y < 0 ) {
            return - x + multiply(x, y + 1);
        }
        else {
            return x + multiply(x, y - 1);
        }
    }

    public static int add(int x, int y) {
        if (y > 0) {
            return add(x, y - 1) + 1;
        } else if (y < 0) {
            return add(x, y+1) - 1;
        } else {
            return x;
        }
    }

    public static int power(int x, int y) {
        if (y < 0) {
            throw new IllegalArgumentException("Illegal Power Argument");
        }
        if (y == 0) {
            return 1;
        } else {
            return x * power(x, y - 1);
        }
    }

    public static int subtraction(int x, int y) {
        if (y > 0) {
            return subtraction(x, y - 1) - 1;
        } else if (y < 0) {
            return subtraction(x, y + 1) + 1;
        } else {
            return x;
        }
    }

}
